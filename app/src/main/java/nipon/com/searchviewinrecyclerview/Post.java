package nipon.com.searchviewinrecyclerview;

public class Post {
    private String titile;
    private String body;

    public Post() {
    }

    public Post(String titile, String body) {
        this();
        this.titile = titile;
        this.body = body;
    }

    public String getTitile() {
        return titile;
    }

    public String getBody() {
        return body;
    }
}
